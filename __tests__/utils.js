const { validateArea, matchText } = require('../helpers/utils');

describe('validateArea', () => {
  it('Should return true if arr is empty', () => {
    const arr = [];
    const arr2 = ['test', 'Test2'];
    const result = validateArea(arr, arr2);
    expect(result).toBe(true);
  });

  it('Should return true if arr contains a match with the other arr', () => {
    const arr = ['no', 'si', 'Test2'];
    const arr2 = ['test', 'Test2'];
    const result = validateArea(arr, arr2);
    expect(result).toBe(true);
  });

  it('Should return false if arr does not contain a match with the other arr', () => {
    const arr = ['no', 'si', 'Test23'];
    const arr2 = ['test', 'Test2'];
    const result = validateArea(arr, arr2);
    expect(result).toBe(false);
  });
});

describe('matchText', () => {
  it('Should return null if boths words do not match', () => {
    const word1 = 'Amanda';
    const word2 = 'Margarita';
    const result = matchText(word1, word2);
    expect(result).toBeNull();
  });

  it('Should contain the matching word', () => {
    const word1 = 'Amanda';
    const word2 = 'Amanda';
    const result = matchText(word1, word2);
    expect(result).toContain('Amanda');
  });

  it('Should not be sensitive', () => {
    const word1 = 'AMANDA';
    const word2 = 'Amanda';
    const result = matchText(word1, word2);
    expect(result).toContain('Amanda');
  });
});
