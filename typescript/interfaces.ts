// redux interfaces

export interface UserState {
  isAuthenticated: boolean;
  user: {
    token: string;
    employee_code: string;
  };
}

export interface IState {
  user: UserState;
  modal: { open: boolean };
}
