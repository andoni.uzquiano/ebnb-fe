import React, { FC } from 'react';
import Router from 'next/router';
import '../styles/globals.css';
import { wrapper } from '@store/index';
import type { AppProps /*, AppContext */ } from 'next/app';
import PublicLayout from '@layouts/PublicLayout';
import '@styles/Calendar.css';
import CustomHead from '@modules/CustomHead';

Router.events.on('routeChangeComplete', () => {
  window.scroll({
    top: 0,
    left: 0,
  });
});

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => (
  <PublicLayout>
    <>
      <CustomHead />
      <Component {...pageProps} />
    </>
  </PublicLayout>
);

export default wrapper.withRedux(WrappedApp);
