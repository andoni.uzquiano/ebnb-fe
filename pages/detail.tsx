import { useState } from 'react';
import Head from 'next/head';
import CustomCalendar from '@elements/CustomCalendar';
import Button from '@elements/Button';
import servicios from '@config/servicios.json';
import amenidades from '@config/amenidades.json';
import FormContainer from '@modules/FormContainer';
import Input from '@elements/Input';
import StartIcon from '../components/icons/StartIcon';
import DetailImages from '@modules/DetailImages';
import getWS from '@hooks/getWS';
import detailImages from '@config/detailImages.json';

const Detail = () => {
  const [date, setDate] = useState<Date | Date[]>(new Date());
  const [maxServices, setMaxServices] = useState(4);
  const { width } = getWS();
  const maxImages = width > 950 ? 5 : 3;

  return (
    <div className="mx-4 sm:mx-8 2xl:mx-0 mt-7">
      <Head>
        <title>Details - Ebnb</title>
      </Head>
      <h1 className="poppins-3xl-semibold">Booster side</h1>
      <div className="flex items-center">
        <StartIcon />
        <span className="inter-sm-semibold ml-1">4.35</span>
        <p className="inter-sm-normal text-e-gray-500 ml-1">
          (7 Reseñas) • Tech Hub, 2da Planta
        </p>
      </div>
      <DetailImages
        className="mb-16 mt-6"
        detailImages={detailImages.slice(0, maxImages)}
      />
      <div className="flex items-start flex-col-reverse md:flex-row">
        <div className="pr-10 lg:pr-24">
          <h2 className="poppins-3xl-semibold">Descubre salas cerca de ti</h2>
          <div className="inter-sm-normal text-e-gray-500">
            Para 7 personas • Aire acondicionado • 1 Televisor 1 • Pizarrón de
            plumón
          </div>
          <div className="w-full h-0.5 bg-gray-200 my-12" />
          {amenidades.map(({ title, src, description }) => (
            <div className="flex justify-start items-start my-4" key={title}>
              <img src={src} alt={title} />
              <div className="ml-4">
                <h2 className="poppins-xl-medium">{title}</h2>
                <p className="inter text-e-gray-500">{description}</p>
              </div>
            </div>
          ))}
          <div className="w-full h-0.5 bg-gray-200 my-12" />
          <h2 className="poppins-2xl-medium">Servicios</h2>
          <div className="flex flex-wrap justify-between w-full my-4">
            {servicios.slice(0, maxServices).map((servicio) => (
              <div key={servicio.id} className="flex items-center w-1/2 my-3">
                <img src={servicio.src} alt={servicio.title} />
                <h3 className="ml-4 inter-sm-normal">{servicio.title}</h3>
              </div>
            ))}
          </div>
          <Button
            hover="hover:bg-gray-200"
            action={() => setMaxServices((prev) => (prev > 4 ? 4 : 24))}
            secondary
          >
            {maxServices < 5 ? 'Mosrar los 24 servicios' : 'Ocultar'}
          </Button>
          <div className="w-full h-0.5 bg-gray-200 my-12" />
          <h2 className="poppins-2xl-medium">
            Seleccionar la fecha de reserva
          </h2>
          <h3 className="mb-5 inter text-e-gray-500">
            Agrega la/s fecha/s que deseas reservar tu sala.
          </h3>
          <CustomCalendar
            date={date}
            setDate={setDate}
            showDoubleView={width >= 800}
            className="mb-16"
          />
        </div>
        <FormContainer
          className="w-full md:w-108 mb-16"
          options={[{ title: 'Salas', list: ['Correr', 'Hablar con amgigos'] }]}
          current={{ title: 'Salas', list: ['Correr', 'Hablar con amgigos'] }}
          inputs={[
            <Input
              name="llegada"
              inputClass="rounded-xl"
              placeholder="LLegada"
            />,
            <Input
              name="salida"
              inputClass="rounded-xl"
              placeholder="Salida"
            />,
            <Input
              name="participantes"
              inputClass="rounded-xl"
              placeholder="Participantes"
            />,
          ]}
        >
          <div className="flex justify-between items-center">
            <div>
              <span className="poppins-semibold">Disponible</span>{' '}
              <span className="inter-sm-normal">/ desde 3:00 p.m.</span>
            </div>
            <div className="flex items-center">
              <StartIcon />
              <span className="inter-sm-semibold ml-1">4.35</span>
            </div>
          </div>
        </FormContainer>
      </div>
    </div>
  );
};

export default Detail;
