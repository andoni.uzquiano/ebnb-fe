import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import BgImage from '@elements/BgImage';
import FormContainer from '@modules/FormContainer';
import options from '@config/options.json';
import Input from '@elements/Input';
import salas from '@config/salas.json';
import MiniCard from '@modules/MiniCard';
import { ApolloCache, InMemoryCache, gql, ApolloClient } from '@apollo/client';
import Button from '@elements/Button';
import experiences from '@config/experiences.json';
import getWS from '@hooks/getWS';
import NextLink from '@elements/NextLink';

interface IHOME {
  data: string[];
}

const Home = ({ data }: IHOME) => {
  const [llegadaSala, setLlegadaSala] = useState('');
  const [salidaSala, setSalidaSala] = useState('');
  const [participantesSala, setParticipantesSala] = useState('');
  const [horaIdaAmenidades, setHoraIdaAmenidades] = useState('');
  const [participantesExp, setParticipantesExp] = useState('');
  const [option, setOption] = useState<string>(options[0].title);
  const current = options.find((item) => item.title === option);
  const { width } = getWS();
  const maxSalas = width > 768 ? 8 : width > 620 ? 6 : 4;
  const router = useRouter();

  const mapOptions = new Map(
    options.map(({ title }) => {
      switch (title) {
        case 'Salas':
          return [
            title,
            [
              <Input
                name="llegada"
                placeholder="Llegada"
                inputClass="rounded-xl"
                value={llegadaSala}
                setValue={setLlegadaSala}
              />,
              <Input
                name="salida"
                placeholder="Salida"
                inputClass="rounded-xl"
                value={salidaSala}
                setValue={setSalidaSala}
              />,
              <Input
                type="number"
                name="participantes"
                placeholder="Participantes"
                inputClass="rounded-xl"
                value={participantesSala}
                setValue={setParticipantesSala}
              />,
            ],
          ];
        case 'Amenidades':
          return [
            title,
            [
              <Input
                name="salida"
                placeholder="Hora de ida"
                inputClass="rounded-xl"
                value={horaIdaAmenidades}
                setValue={setHoraIdaAmenidades}
              />,
            ],
          ];
        case 'Experiencias':
          return [
            title,
            [
              <Input
                name="salida"
                placeholder="Número de participantes"
                inputClass="rounded-xl"
                value={participantesExp}
                setValue={setParticipantesExp}
              />,
            ],
          ];
        default:
          return [
            title,
            [
              <Input
                name="default"
                placeholder="default"
                inputClass="rounded-xl"
              />,
            ],
          ];
      }
    })
  );

  return (
    <>
      <Head>
        <title>Home - Ebnb</title>
      </Head>
      {width < 768 && (
        <FormContainer
          className="bg-white w-full"
          options={options}
          setOption={setOption}
          inputs={mapOptions.get(current?.title || options[0].title) || []}
          current={current || { title: options[0].title, list: [] }}
        />
      )}

      <BgImage
        src="/assets/images/hero_section.jpeg"
        alt="hero section"
        width="100%"
        height="659px"
        childrenClass="top-20 left-36"
        priority
      >
        {width >= 768 ? (
          <FormContainer
            className="bg-white w-108"
            options={options}
            setOption={setOption}
            inputs={mapOptions.get(current?.title || options[0].title) || []}
            current={current || { title: options[0].title, list: [] }}
          />
        ) : (
          <div></div>
        )}
      </BgImage>
      <div className="my-14 x-margins">
        <h2 className="poppins-3xl-semibold mb-14">
          Descubre salas cerca de ti
        </h2>
        <div className="flex flex-wrap justify-between mb-12">
          {salas.slice(0, maxSalas).map(({ img, title, text }, i) => (
            <NextLink
              key={title}
              to="/detail"
              className={`flex min-w-200 ${
                (i + 1) % 4 === 0 ? 'w-1/10' : 'w-1/4'
              }`}
            >
              <MiniCard
                className="apply-img-rounded mr-1 mb-6"
                src={img}
                title={title}
                text={text}
                alt={title}
              />
            </NextLink>
          ))}
        </div>
        <BgImage
          className="apply-img-rounded"
          src="/assets/images/host.jpeg"
          alt="hero section"
          width="100%"
          height="412px"
          childrenClass="top-20 left-5 sm:left-20"
          gradient
        >
          <div className="w-full sm:w-105">
            <h2 className="poppins-5xl-semibold text-white w-full sm:w-105">
              Conviértete en todo un host
            </h2>
            <p className="text-white inter mt-1 mb-6">
              Crea y Reserva las reuniones con tus compañeros para trabajar de
              una manera más comoda.
            </p>
            <Button action={() => router.push('/search')} secondary>
              Reserva una sala
            </Button>
          </div>
        </BgImage>
        <h2 className="poppins-3xl-semibold mt-14">Descubre experiencias</h2>
        <p className="poppins-2xl">
          Un espacio de trabajo no solo para el trabajo.
        </p>
        <div className="flex w-full justify-between mt-8 flex-wrap">
          {experiences.map((exp) => (
            <NextLink to="/detail" key={exp.title} className="w-full lg:w-auto">
              <article className="w-full lg:w-auto m-1 mt-2">
                <BgImage
                  src={exp.img}
                  alt={exp.title}
                  height="296px"
                  width={`${width <= 1024 ? '100%' : '405px'}`}
                  className="apply-img-rounded"
                />
                <h2 className="poppins-xl-medium mt-4">{exp.title}</h2>
                <h3 className="inter">{exp.text}</h3>
              </article>
            </NextLink>
          ))}
        </div>
      </div>
    </>
  );
};

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: 'https://ebnb-be.herokuapp.com/graphql',
    cache: new InMemoryCache(),
  });

  const { data } = await client.query({
    query: gql`
      query GetSalas {
        features {
          name
        }
      }
    `,
  });

  return {
    props: {
      data: [],
    },
  };
}

export default Home;
