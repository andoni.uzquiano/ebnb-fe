import React from 'react';
import Head from 'next/head';
import Link from '@elements/Link';
import areas from '@config/areas.json';
import MainCard from '@modules/MainCard';
import { useState, useEffect } from 'react';
import { validateArea, matchText } from '@helpers/utils';
import getWS from '@hooks/getWS';
import router from 'next/router';
import {
  ApolloClient,
  gql,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { useSelector } from 'react-redux';
import { IState } from '../typescript/interfaces';

interface ISearch {
  features: string[];
  spaces: {
    id: string;
    name: string;
    features: string[];
    image: string;
    place: string;
    capacity: number;
  }[];
}

const Search = ({ features, spaces }: ISearch) => {
  const [current, setCurrent] = useState<string[]>([]);
  const [on, setOn] = useState(false);
  const { width } = getWS();
  const [liked, setLiked] = useState<string[]>([]);
  const [userId, setUserId] = useState('');
  const search = router.router?.query.search || '';
  const { user } = useSelector((state: IState) => state);

  const handleCurrent = (target: string) => {
    if (current.includes(target)) {
      setCurrent((prev) => prev.filter((item) => item !== target));
    } else {
      setCurrent((prev) => [...prev, target]);
    }
  };

  const httpLink = createHttpLink({
    uri: 'https://ebnb-be.herokuapp.com/graphql',
  });

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: user.user.token ? `Bearer ${user.user.token}` : '',
      },
    };
  });

  useEffect(() => {
    if (user.isAuthenticated) {
      const client = new ApolloClient({
        link: authLink.concat(httpLink),
        cache: new InMemoryCache(),
      });
      client
        .query({
          variables: { employee_code: user.user.employee_code },
          query: gql`
            query GerUser($employee_code: String!) {
              user(
                getUserInput: { key: EMPLOYEE_CODE, value: $employee_code }
              ) {
                id
                liked_spaces {
                  id
                }
              }
            }
          `,
        })
        .then((res) => {
          setLiked(
            res.data.user.liked_spaces.map((item: { id: string }) => item.id)
          );
          setUserId(res.data.user.id);
        })
        .catch((error) => console.log(error));
    } else {
      setLiked([]);
    }
  }, [user.isAuthenticated]);

  return (
    <div className="mx-4 sm:mx-8 2xl:mx-0">
      <Head>
        <title>Search - Ebnb</title>
      </Head>
      <div className="mt-14">
        <span className="inter-sm-normal">{`${spaces.length} Resultados`}</span>
        <h2 className="poppins-3xl-bold mt-2 mb-6">Salas en 2da Planta</h2>
        {width < 640 && (
          <button
            onClick={() => setOn((prev) => !prev)}
            type="button"
            className={`border border-black rounded-full py-2 px-5 mr-2 mt-1 ${
              on ? 'bg-black text-white hover:bg-gray-500' : 'hover:bg-gray-100'
            }`}
          >
            Filtros
          </button>
        )}
        {(width >= 640 || on) && (
          <>
            {features.map((filter) => (
              <button
                key={filter}
                onClick={() => handleCurrent(filter)}
                type="button"
                className={`border border-black rounded-full py-2 px-5 mr-2 mt-1 ${
                  current.includes(filter)
                    ? 'bg-black text-white hover:bg-gray-500'
                    : 'hover:bg-gray-100'
                }`}
              >
                {filter}
              </button>
            ))}
          </>
        )}
        <p className="inter-sm-normal mt-8">
          Antes de reservar, revisa las restricciones relacionadas con el
          coronavirus.{' '}
          <Link to="/" className="font-semibold hover:border-black border-b">
            Más información
          </Link>
        </p>
      </div>
      <div className="bg-gray-200 w-1/2 h-0.5 my-6" />
      <div className="mb-16">
        {spaces
          .filter(
            ({ features, name }) =>
              validateArea(current, features) && matchText(String(search), name)
          )
          .map(({ id, image, name, place, capacity, features }, i) => (
            <React.Fragment key={name}>
              <MainCard
                id={id}
                src={image}
                alt={name}
                title={name}
                features={features}
                place={place}
                capacity={capacity}
                available={true}
                rate="4.5"
                reviews={10}
                liked={liked.includes(id)}
                userId={userId}
              />
              {areas[i + 1] && (
                <div className="bg-gray-200 w-full h-0.5 my-6" />
              )}
            </React.Fragment>
          ))}
      </div>
    </div>
  );
};

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: 'https://ebnb-be.herokuapp.com/graphql',
    cache: new InMemoryCache(),
  });

  try {
    const {
      data: { features },
    } = await client.query({
      query: gql`
        query GetSalas {
          features {
            name
          }
        }
      `,
    });

    const {
      data: { spaces },
    } = await client.query({
      query: gql`
        query GetSalas {
          spaces {
            id
            name
            image
            capacity
            place
            features {
              name
            }
          }
        }
      `,
    });

    const sendSpaces = spaces.map((space: any) => {
      const clone = { ...space };
      clone.features = space.features.map((item: any) => item.name);
      return clone;
    });

    return {
      props: {
        features: features.map((item: { name: string }) => item.name),
        spaces: sendSpaces,
      },
    };
  } catch (error) {}

  return {
    props: {
      features: [],
      spaces: [],
    },
  };
}

export default Search;
