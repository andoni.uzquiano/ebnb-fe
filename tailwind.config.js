module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './layouts/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false,
  theme: {
    extend: {
      borderWidth: {
        9: '16px',
      },
      margin: {
        'r-images': '0 1vw 0 0',
      },
      width: {
        105: '415px',
        108: '419px',
        116: '510px',
      },
      height: {
        116: '536px',
      },
      borderColor: {
        DEFAULT: '#C7C7C7',
      },
      maxWidth: {
        100: '1440px',
      },
      minHeight: {
        '83vw': '83vh',
      },
      minWidth: {
        200: '200px',
      },
      fontFamily: {
        nunito: ['Nunito'],
        inter: ['Inter'],
        poppins: ['Poppins'],
        roboto: ['roboto'],
      },
      colors: {
        'e-pink': '#ED198E',
        'e-black': '#121212',
        'e-gray': {
          300: '#C7C7C7',
          400: '#939393',
          500: '#6D6D6D',
        },
        'e-t-black': 'RGBA(0, 0, 0, 0.5)',
      },
    },
    keyframes: {
      spin: {
        '0%': { transform: 'rotate(0deg)' },
        '100%': { transform: 'rotate(1turn)' },
      },
    },
    animations: {
      spin: 'spin 1s ease-in-out infinite',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
