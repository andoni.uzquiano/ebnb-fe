export const validateArea = (arr1: string[], arr2: string[]) => {
  if (arr1.length === 0) return true;
  for (const arr of arr1) {
    if (arr2.includes(arr)) return true;
  }

  return false;
};

export const matchText = (
  look: string,
  text: string
): RegExpExecArray | null => {
  const reg = new RegExp(look, 'i');
  return reg.exec(text);
};
