export default {
  required: {
    value: true,
    message: 'This field is required',
  },
  minLength: (value: number) => ({
    value,
    message: `Min characters allowed are ${value}`,
  }),
  maxLength: (value: number) => ({
    value,
    message: `Max characters allowed are ${value}`,
  }),
  email: {
    value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/,
    message: 'Invalid email',
  },
  noEmptySpace: {
    value: /^[^\s].*/,
    message: 'No blanks at the beginning',
  },
  letters: {
    value: /^[A-Za-z0-9., áéíóúñÁÉÍÓÚÑ]*$/,
    message: 'No special characters',
  },
  decimal: {
    value: /^[0-9]*(\.\d{0,2})?$/,
    message: 'Only 2 decimals are allowed',
  },
  number: {
    value: /^[0-9]*$/,
    message: 'Only numbers are allowed',
  },
  phone: {
    value: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
    message: 'Should be a valid phone number',
  },
  checkLength: (value: string) => value.split(' ')[0].includes('@') || value.split(' ').every((item) => item.length < 29) || 'Maximum number of characters exceeded',
  checkAmount: (value: number) => value > 0 || 'Amount must be greater than 0',
  checkNumbers: (value: string) => /^[0-9].*$/.test(value) || 'Only numbers are allowed',
  checkDecimals: (value: string) => /^[0-9]*(\.\d{0,2})?$/.test(value) || 'Only 2 decimals are allowed',
  checkUsername: (value: string) => /^[A-Za-z0-9]*$/.test(value) || 'Use just letters and numbers for username',
  checkDate: (value: Date) => new Date(value) > new Date(new Date().setDate(new Date().getDate() - 1))
        || 'The deadline must be after today at least',
  oneOrMoreUpperCase: (value: string) => /[A-Z]+/.test(value) || 'Must contains at least one uppercase letter',
  oneOrMoreLowerCase: (value: string) => /[a-z]+/.test(value) || 'Must contains at least one lowercase letter',
  oneOrMoreNumbers: (value: string) => /[0-9]+/.test(value) || 'Must contains at least one number',
};
