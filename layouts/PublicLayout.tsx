import Footer from '@modules/Footer';
import NavBar from '@modules/NavBar';

interface IPublickLayout {
  children: JSX.Element;
}

const PublicLayout: React.FC<IPublickLayout> = ({ children }) => (
  <main className="max-w-100 mx-auto">
    <NavBar />
    <div className="mt-24 min-h-83vw">{children}</div>
    <Footer />
  </main>
);

export default PublicLayout;
