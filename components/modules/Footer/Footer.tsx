import Link from '../../elements/Link';

const Footer = () => (
  <footer className="roboto p-4 text-center">
    © ebnb.inc, 2021 • Todos los Derechos Reservados • Proyecto basado en
    <Link
      to="https://es.airbnb.com/"
      className="text-e-pink hover:border-e-pink"
    >
      airbnb.com
    </Link>
  </footer>
);

export default Footer;
