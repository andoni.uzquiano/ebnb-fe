import Head from 'next/head';
import PropTypes from 'prop-types';

interface ICustomHead {
  title?: string;
  keys?: string;
  description?: string;
  url?: string;
  urlImg?: string;
  imgType?: string;
  external?: boolean;
}

const CustomHead: React.FC<ICustomHead> = ({
  title,
  keys,
  description,
  url,
  urlImg,
  imgType,
  external,
}) => (
  <Head>
    <link rel="manifest" href="/manifest.json" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#FFB74F" />
    <meta key="name" name="name" content={title} />
    <meta name="keywords" content={keys} />
    <meta key="description" name="description" content={description} />
    <meta key="og-url" property="og:url" content={url} />
    <meta key="og-title" property="og:title" content={title} />
    <meta property="og:type" content="website" />
    <meta
      key="og-description"
      property="og:description"
      content={String(description)}
    />

    <meta
      key="og-image"
      property="og:image"
      content={external ? urlImg : `${url}${urlImg}`}
    />
    <meta
      key="og-image-type"
      property="og:image:type"
      content={`image/${imgType}`}
    />
    <meta key="twitter-title" name="twitter:title" content={title} />
    <meta name="twitter:card" content="website" />
    <meta
      key="twitter-image"
      name="twitter:image"
      content={`${url}${urlImg}`}
    />
    <meta
      key="twitter-description"
      name="twitter:description"
      content={String(description)}
    />
    <title>{title}</title>
    <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
    <link rel="alternate icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="/logo192.png" />
  </Head>
);

CustomHead.defaultProps = {
  title: 'Ebnb',
  keys: 'elaniin, ebnb, booking, fun',
  // eslint-disable-next-line
  description: 'Ebnb a place where you can be your own place!',
  url: process.env.NEXT_PUBLIC_URL,
  urlImg: '/logo512.png',
  imgType: 'png',
  external: false,
};

CustomHead.propTypes = {
  title: PropTypes.string,
  keys: PropTypes.string,
  description: PropTypes.string,
  url: PropTypes.string,
  urlImg: PropTypes.string,
  imgType: PropTypes.string,
  external: PropTypes.bool,
};

export default CustomHead;
