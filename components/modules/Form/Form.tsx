import React from 'react';
import PropTypes from 'prop-types';
interface IForm {
  list: string[];
  title: string;
  inputs: JSX.Element[];
  className?: string;
  defaultO?: string | null;
}

const Form: React.FC<IForm> = ({
  className,
  list,
  title,
  inputs,
  defaultO,
}) => (
  <div className={`${className} border rounded-xl`}>
    <div className="border-b relative">
      <div className="ml-3 mt-2 inter-xm-normal text-e-gray-500">{title}</div>
      <select className="rounded-xl w-full p-2 border-transparent bg-transparent">
        {defaultO && <option>{defaultO}</option>}
        {list.map((item) => (
          <option key={item} value={item}>
            {item}
          </option>
        ))}
      </select>
    </div>
    <div className="flex w-full flex-wrap">
      {inputs.map((input, i) => (
        <div
          key={i}
          className={
            inputs[i + 1]
              ? `${inputs.length > 2 ? 'w-1/2' : 'w-full'} ${
                  i === 0 ? 'border-r' : ''
                } border-b`
              : 'w-full'
          }
        >
          {input}
        </div>
      ))}
    </div>
  </div>
);

Form.defaultProps = {
  className: '',
  defaultO: null,
};

Form.propTypes = {
  list: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  title: PropTypes.string.isRequired,
  inputs: PropTypes.arrayOf(PropTypes.element.isRequired).isRequired,
  className: PropTypes.string,
  defaultO: PropTypes.string,
};

export default Form;
