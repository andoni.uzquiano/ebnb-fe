import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import DetailImages from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const detailImages = shallow(
      <DetailImages detailImages={['/assets/images/host']} />
    );
    expect(shallowToJson(detailImages)).toMatchSnapshot();
  });
});
