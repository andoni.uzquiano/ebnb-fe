import React from 'react';
import PropTypes from 'prop-types';
import BgImage from '../../elements/BgImage';

interface IDetailImages {
  className?: string;
  detailImages: string[];
}

const DetailImages: React.FC<IDetailImages> = ({ className, detailImages }) => (
  <div className={`${className} w-full h-116 flex flex-wrap`}>
    <div
      className={`${detailImages.length > 3 ? 'w-1/2 h-full' : 'w-full h-1/2'}`}
    >
      <BgImage
        key={detailImages[0]}
        src={detailImages[0]}
        alt="Details"
        height="100%"
        width="100%"
        className={`${
          detailImages.length > 3
            ? 'apply-img-left-rounded'
            : 'apply-img-top-rounded'
        }`}
      />
    </div>
    <div
      className={`${
        detailImages.length > 3 ? 'w-1/2' : 'w-full'
      } h-full flex flex-wrap`}
    >
      {detailImages.map((img, i) => (
        <React.Fragment key={img}>
          {i > 0 && (
            <BgImage
              key={img}
              src={img}
              alt="Details"
              width={`${detailImages.length > 3 ? '48%' : '49.4%'}`}
              height="49.2%"
              className={`${
                i === 2
                  ? `${
                      detailImages.length > 3
                        ? 'apply-img-top-right-rounded mb-2'
                        : 'apply-img-bottom-right-rounded mt-2'
                    }`
                  : `${
                      i === 4
                        ? 'apply-img-bottom-right-rounded'
                        : `${
                            detailImages.length > 3
                              ? 'mx-2'
                              : 'apply-img-bottom-left-rounded mt-2 m-r-images'
                          }`
                    }`
              }`}
            />
          )}
        </React.Fragment>
      ))}
    </div>
  </div>
);

DetailImages.defaultProps = {
  className: '',
};

DetailImages.propTypes = {
  className: PropTypes.string,
  detailImages: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export default DetailImages;
