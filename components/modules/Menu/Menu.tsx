import NextLink from '@elements/NextLink';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { IState } from '../../../typescript/interfaces';
import { signOut } from '@store/actions';

interface IMenu {
  action: () => void;
}

const Menu: React.FC<IMenu> = ({ action }) => {
  const { user } = useSelector((state: IState) => state);
  const dispatch = useDispatch();

  return (
    <ul className="w-56 absolute top-14 right-0 bg-white rounded-xl shadow-xl border border-gray-200">
      <li>
        <button
          onClick={user.isAuthenticated ? () => dispatch(signOut()) : action}
          type="button"
          className="p-4 inter-sm-semibold border-b w-full text-left border-gray-200 hover:bg-e-pink hover:text-white rounded-t-xl"
        >
          {user.isAuthenticated ? 'Cierra sesión' : 'Inicia sesión'}
        </button>
      </li>
      <li>
        <NextLink
          className="block p-4 inter-sm-semibold hover:bg-e-pink hover:text-white"
          to="/search"
        >
          Ver Salas Cercanas
        </NextLink>
      </li>
      <li>
        <NextLink
          className="block p-4 inter-sm-semibold hover:bg-e-pink hover:text-white"
          to="/search"
        >
          Organizar una experiencia
        </NextLink>
      </li>
      <li>
        <NextLink
          className="block p-4 inter-sm-semibold hover:bg-e-pink rounded-b-xl hover:text-white"
          to="/"
        >
          Ayuda
        </NextLink>
      </li>
    </ul>
  );
};

Menu.propTypes = {
  action: PropTypes.func.isRequired,
};

export default Menu;
