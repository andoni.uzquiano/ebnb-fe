import BgImage from '../../elements/BgImage';
import PropTypes from 'prop-types';

interface IMiniCard {
  className?: string;
  src: string;
  alt: string;
  title: string;
  text: string;
}

const MiniCard: React.FC<IMiniCard> = ({
  className,
  src,
  alt,
  title,
  text,
}) => (
  <div className={`flex items-center ${className} min-w-200`}>
    <BgImage
      src={src}
      alt={alt}
      width="64px"
      height="64px"
      className="my-image"
    />
    <div className="ml-4">
      <h2 className="inter-semibold-2">{title}</h2>
      <h3 className="inter text-e-gray-500">{text}</h3>
    </div>
  </div>
);

MiniCard.defaultProps = {
  className: '',
};

MiniCard.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default MiniCard;
