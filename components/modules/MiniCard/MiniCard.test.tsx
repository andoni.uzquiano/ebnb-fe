import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import MiniCard from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const miniCard = shallow(<MiniCard src="" alt="" title="" text="" />);
    expect(shallowToJson(miniCard)).toMatchSnapshot();
  });
});
