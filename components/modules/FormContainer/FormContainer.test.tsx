import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import FormContainer from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const formCont = shallow(
      <FormContainer
        options={[]}
        inputs={[]}
        current={{ title: 'testing', list: [] }}
      />
    );
    expect(shallowToJson(formCont)).toMatchSnapshot();
  });
});
