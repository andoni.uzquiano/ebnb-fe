import PropTypes from 'prop-types';
import Form from '../Form';
import Button from '../../elements/Button';
import { Dispatch, SetStateAction } from 'react';
import router from 'next/router';

interface IFormContainer {
  options: {
    title: string;
    list: string[];
  }[];
  current: {
    title: string;
    list: string[];
  };
  inputs: JSX.Element[];
  className?: string;
  setOption?: Dispatch<SetStateAction<string>> | null;
  children?: JSX.Element | null;
}

const FormContainer: React.FC<IFormContainer> = ({
  options,
  className,
  inputs,
  setOption,
  current,
  children,
}) => (
  <div className={`${className} p-6 rounded-2xl shadow-xl`}>
    {children ? (
      children
    ) : (
      <>
        <h2 className="poppins-4xl-semibold">Comienza una nueva aventura</h2>
        <p className="inter mt-1">
          Busca una sala donde puedas sentirte comodo trabajando y compartiendo.
        </p>
      </>
    )}
    {setOption && (
      <div className="flex w-full mt-4 justify-around">
        {options.map(({ title }) => (
          <button
            className={`w-full border-b py-2 ${
              title === current.title ? 'border-black' : 'border-gray-200'
            }`}
            type="button"
            onClick={() => setOption(title)}
            key={title}
          >
            {title}
          </button>
        ))}
      </div>
    )}
    <Form
      className="mt-4"
      list={current.list}
      title="Actividad"
      inputs={inputs}
      defaultO={`${
        inputs.length < 2 ? '¿Qué deseas hacer?' : '¿Que actividad realizaras?'
      }`}
    />
    {!children ? (
      <p className="inter-xm-normal text-e-gray-400 mt-1 mb-7">
        Estos datos son importantes para darte un mejor resultado de tu
        busqueda.
      </p>
    ) : (
      <div className="my-4" />
    )}
    <Button
      hover={children ? 'w-full justify-center' : ''}
      action={() => router.push('/search')}
    >{`Buscar ${current.title.toLocaleLowerCase()} ${
      current.title === 'Salas' ? 'disponibles' : ''
    }`}</Button>
  </div>
);

FormContainer.defaultProps = {
  className: '',
  setOption: null,
  children: null,
};

FormContainer.propTypes = {
  className: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      list: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    }).isRequired
  ).isRequired,
  current: PropTypes.shape({
    title: PropTypes.string.isRequired,
    list: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  }).isRequired,
  inputs: PropTypes.arrayOf(PropTypes.element.isRequired).isRequired,
  setOption: PropTypes.func,
  children: PropTypes.element,
};

export default FormContainer;
