import React, { useState } from 'react';
import PropTypes from 'prop-types';
import BgImage from '../../elements/BgImage';
import HeartIcon from '../../icons/HeartIcon';
import StartIcon from '../../icons/StartIcon';
import getWS from '../../../hooks/getWS';
import NextLink from '../../elements/NextLink';
import {
  ApolloClient,
  createHttpLink,
  gql,
  InMemoryCache,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { IState } from '@typescript/interfaces';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { handleModal } from '../../../store/actions/index';
interface IMainCard {
  id: string;
  src: string;
  alt: string;
  place: string;
  title: string;
  capacity: number;
  features: string[];
  available: boolean;
  rate: string;
  reviews: number;
  liked: boolean;
  userId?: string;
}

const MainCard: React.FC<IMainCard> = ({
  id,
  src,
  alt,
  place,
  title,
  capacity,
  features,
  available,
  rate,
  reviews,
  liked,
  userId,
}) => {
  const [like, setLike] = useState(liked);
  const { width } = getWS();
  const { user } = useSelector((state: IState) => state);
  const dispatch = useDispatch();

  const httpLink = createHttpLink({
    uri: 'https://ebnb-be.herokuapp.com/graphql',
  });
  if (id === '9540939f-ca27-4c88-849e-996ae5e493f9') console.log(liked);
  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: user.user.token ? `Bearer ${user.user.token}` : '',
      },
    };
  });

  useEffect(() => {
    setLike(liked);
  }, [liked]);

  const handleLiked = () => {
    if (user.isAuthenticated) {
      const client = new ApolloClient({
        link: authLink.concat(httpLink),
        cache: new InMemoryCache(),
      });
      client
        .mutate({
          variables: { user_id: userId, space_id: id },
          mutation: gql`
            mutation LikeSpace($user_id: String!, $space_id: String!) {
              likeSpace(
                likeSpaceInput: { userId: $user_id, spaceId: $space_id }
              ) {
                id
              }
            }
          `,
        })
        .then((res) => {
          setLike((prev) => !prev);
        })
        .catch((error) => console.log(error));
    } else {
      dispatch(handleModal(true));
    }
  };

  return (
    <div className="flex flex-col sm:flex-row">
      <BgImage
        src={src}
        alt={alt}
        width={`${width < 640 ? '100%' : '376px'}`}
        height="216px"
        className="apply-img-rounded"
        childrenClass="top-2 left-2 w-11/12"
      >
        <div className="flex justify-between">
          <div className="bg-white border-black px-2 py-1 rounded-xl inter-sm-semibold">
            {available ? 'DISPONIBLE AHORA' : 'NO DISPONIBLE'}
          </div>
          {width < 640 && (
            <button type="button" onClick={() => setLike((prev) => !prev)}>
              <HeartIcon color={like ? 'red' : 'white'} />
            </button>
          )}
        </div>
      </BgImage>
      <div className="ml-0 mt-8 sm:mt-0 sm:ml-8 flex flex-col justify-between w-full">
        <div className="relative">
          <NextLink to="/detail">
            <>
              <h3 className="inter-sm-medium text-e-gray-500">{place}</h3>
              <h2 className="inter-title-bold">{title}</h2>
            </>
          </NextLink>
          <div className="w-10 h-0.5 bg-e-gray-300 my-3 opacity-50" />
          <div className="inter-sm-normal text-e-gray-400 flex items-center flex-wrap">
            {`Para ${capacity} personas `}
            {features.map((item) => (
              <React.Fragment key={item}>
                <div className="flex ml-1 justify-center items-center w-4 h-4 pt-1">
                  <div>{` *`}</div>
                </div>
                <span>{` ${item} `}</span>
              </React.Fragment>
            ))}
          </div>
          {width >= 640 && (
            <button type="button" onClick={handleLiked}>
              <HeartIcon
                className="absolute top-1 right-1"
                color={like ? 'red' : '#121212'}
              />
            </button>
          )}
        </div>
        <div className="flex justify-between">
          <div className="flex items-center">
            <StartIcon />
            <span className="ml-1 inter-sm-semibold">{rate}</span>
            <span className="ml-1 inter inter-sm-normal text-e-gray-400">{`(${reviews} reseñas)`}</span>
          </div>
          <p className="inter-title-bold">
            {available ? 'Disponible' : 'No Disponible'}
          </p>
        </div>
      </div>
    </div>
  );
};

MainCard.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  place: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  capacity: PropTypes.number.isRequired,
  features: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  available: PropTypes.bool.isRequired,
  rate: PropTypes.string.isRequired,
  reviews: PropTypes.number.isRequired,
  liked: PropTypes.bool.isRequired,
  userId: PropTypes.string,
  id: PropTypes.string.isRequired,
};

export default MainCard;
