import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import MainCard from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const mainCard = shallow(
      <MainCard
        src=""
        alt=""
        place=""
        capacity={2}
        rate=""
        available
        title=""
        features={[]}
        reviews={2}
      />
    );
    expect(shallowToJson(mainCard)).toMatchSnapshot();
  });
});
