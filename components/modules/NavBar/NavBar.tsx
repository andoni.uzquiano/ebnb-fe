import { useDispatch, useSelector } from 'react-redux';
import { signIn } from '@store/actions';
import Input from '../../elements/Input';
import NextLink from '../../elements/NextLink';
import getWS from '../../../hooks/getWS';
import { FormEvent, useState } from 'react';
import router from 'next/router';
import Modal from '../../elements/Modal';
import Form from '@modules/Form';
import Button from '@elements/Button';
import { ApolloClient, gql, InMemoryCache } from '@apollo/client';
import Menu from '../Menu/Menu';
import { handleModal } from '../../../store/actions/index';
import { IState } from '../../../typescript/interfaces';

const NavBar = () => {
  const { scroll } = getWS();
  const [search, setSearch] = useState('');
  const [menu, setMenu] = useState(false);
  const [employeeCode, setEmployeeCode] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [signInerror, setSignInError] = useState<null | string>(null);
  const dispatch = useDispatch();
  const { modal } = useSelector((state: IState) => state);

  const onSubmit = (event: FormEvent) => {
    event.preventDefault();
    router.push({
      pathname: '/search',
      query: { search },
    });
    setSearch('');
  };

  const handleSignIn = async (event: FormEvent) => {
    event.preventDefault();
    setSignInError(null);
    setLoading(true);
    const client = new ApolloClient({
      uri: 'https://ebnb-be.herokuapp.com/graphql',
      cache: new InMemoryCache(),
    });
    try {
      const { data } = await client.query({
        variables: { employeeCode, password },
        query: gql`
          query SignIn($employeeCode: String!, $password: String!) {
            signIn(
              signInInput: { employee_code: $employeeCode, password: $password }
            ) {
              accessToken
            }
          }
        `,
      });
      dispatch(
        signIn({ token: data.signIn.accessToken, employee_code: employeeCode })
      );
      dispatch(handleModal(false));
    } catch (error) {
      setSignInError('Invalid credentials');
    }
    setLoading(false);
  };

  return (
    <nav
      className={`duration-500 transition-shadow w-full px-4 md:px-16 py-5 flex justify-between items-center fixed 
      top-0 left-0 bg-white z-50 ${scroll > 0 ? 'shadow-xl' : ''}`}
    >
      <NextLink to="/">
        <div className="nunito-title text-e-pink flex items-center">
          <img src="/assets/svg/logo.svg" alt="logo" className="mr-2" />
          ebnb
        </div>
      </NextLink>
      <form onSubmit={onSubmit}>
        <Input
          name="search"
          placeholder="Busca una sala"
          icon="/assets/svg/search.svg"
          iconClass="right-2"
          className="hidden md:flex"
          value={search}
          setValue={setSearch}
          submit
        />
      </form>
      <div className="relative flex items-center">
        {menu && <Menu action={() => dispatch(handleModal(true))} />}
        <span className="inter-semibold">Se un host</span>
        <button
          type="button"
          onClick={() => setMenu((prev) => !prev)}
          className="flex items-center ml-3 border p-2 rounded-full hover:opacity-75"
        >
          <img className="w-6" src="/assets/svg/menu.svg" alt="menu" />
          <img className="ml-2 w-8" src="/assets/svg/user.svg" alt="user" />
        </button>
      </div>
      {modal.open && (
        <Modal
          closeAction={() => dispatch(handleModal(false))}
          title="Inicia sesión"
        >
          <form onSubmit={handleSignIn}>
            <Form
              title="Pais/Región"
              list={['El Salvador (+503)']}
              inputs={[
                <Input
                  name="employee_code"
                  inputClass="rounded-xl"
                  placeholder="Código de empleado"
                  value={employeeCode}
                  setValue={setEmployeeCode}
                />,
                <Input
                  name="password"
                  type="password"
                  placeholder="password"
                  inputClass="rounded-xl"
                  value={password}
                  setValue={setPassword}
                />,
              ]}
            />
            <p className="inter-sm-normal mt-1">
              Te estaremos enviando un correo de verificación para continuar.
            </p>
            <p className="inter text-red-500 text-center">{signInerror}</p>
            <Button loading={loading} hover="w-full justify-center mt-4" submit>
              Continuar
            </Button>
            <div className="flex items-center">
              <div className="w-full h-0.5 bg-gray-300" />
              <div className="mx-2 my-6">o</div>
              <div className="w-full h-0.5 bg-gray-300" />
            </div>
            <div className="flex justify-between items-center">
              <button
                className="flex bg-black text-white rounded-full px-2 py-1 items-center hover:bg-opacity-60"
                type="button"
              >
                <img className="mr-2" src="/assets/svg/mail.svg" alt="mail" />
                Continua con correo electrónico
              </button>
              <button type="button">
                <img src="/assets/svg/google.svg" alt="google" />
              </button>
              <button type="button">
                <img src="/assets/svg/facebook.svg" alt="facebook" />
              </button>
              <button type="button">
                <img src="/assets/svg/apple.svg" alt="apple" />
              </button>
            </div>
            <p className="mt-10 inter">
              ¿No conoces tu código de empleado?{' '}
              <NextLink
                className="inter-base-semibold border-transparent border-b hover:border-black"
                to="/"
              >
                Solicitalo aquí
              </NextLink>
            </p>
          </form>
        </Modal>
      )}
    </nav>
  );
};

export default NavBar;
