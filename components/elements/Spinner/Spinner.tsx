import PropTypes from 'prop-types';

interface ISpinner {
  className: string;
}

const Spinner = ({ className }: ISpinner) => (
  <div
    className={`${className} animate-spin w-5 h-5 bg-transparent rounded-full border-white border-t-2 border-l-2`}
  />
);

Spinner.defaultProps = {
  className: '',
};

Spinner.propTypes = {
  className: PropTypes.string,
};

export default Spinner;
