import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import Spinner from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const spinner = shallow(<Spinner />);
    expect(shallowToJson(spinner)).toMatchSnapshot();
  });
});
