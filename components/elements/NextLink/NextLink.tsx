import PropTypes from 'prop-types';
import Link from 'next/link';

interface INextLink {
  className?: string;
  to: string;
  children: string | JSX.Element;
}

const NextLink: React.FC<INextLink> = ({ className, to, children }) => (
  <Link href={to}>
    <a className={className}>{children}</a>
  </Link>
);

NextLink.defaultProps = {
  className: '',
};

NextLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element.isRequired,
    PropTypes.string.isRequired,
  ]).isRequired,
};

export default NextLink;
