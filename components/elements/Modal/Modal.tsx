import { useState } from 'react';
import PropTypes from 'prop-types';

interface IModal {
  children: JSX.Element;
  className?: string;
  title: string;
  closeAction: () => void;
}

const Modal: React.FC<IModal> = ({
  children,
  className,
  title,
  closeAction,
}) => {
  const [open, setOpen] = useState(true);

  return (
    <div
      onClick={closeAction}
      className="fixed w-screen h-screen bg-e-t-black top-0 left-0 flex justify-center items-center"
    >
      <div
        onClick={(e) => e.stopPropagation()}
        className={`bg-white rounded-xl w-full ml-2 mr-6 sm:w-116 cursor-default ${className}`}
        onKeyDown={(e) => e.stopPropagation()}
        role="button"
        tabIndex={0}
      >
        <div className="relative w-full flex items-center justify-center border-b p-3">
          <button
            className="absolute left-4 top-1 w-10 h-10 text-4xl"
            type="button"
            onClick={closeAction}
          >
            <img src="/assets/svg/close.svg" alt="close" />
          </button>
          <h2 className="inter-base-semibold">{title}</h2>
        </div>
        <div className="p-6">{children}</div>
      </div>
    </div>
  );
};

Modal.defaultProps = {
  className: '',
};

Modal.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  closeAction: PropTypes.func.isRequired,
};

export default Modal;
