import React, {
  Dispatch,
  FocusEvent,
  FunctionComponent,
  SetStateAction,
} from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

interface IInput {
  name: string;
  type?: string;
  placeholder?: string;
  label?: string;
  classLabel?: string;
  inputClass?: string;
  error?: { message?: string };
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  icon?: string | null;
  iconAction?: (() => {}) | null;
  alt?: string;
  iconClass?: string;
  setValue?: Dispatch<SetStateAction<string>>;
  value?: string;
  className?: string;
  submit?: boolean;
}

const Input: FunctionComponent<IInput> = ({
  name,
  type,
  placeholder,
  label,
  classLabel,
  inputClass,
  error,
  onChange,
  onBlur,
  icon,
  iconAction,
  iconClass,
  alt,
  setValue,
  value,
  className,
  submit,
}) => {
  const mainBlur = (e: FocusEvent<HTMLInputElement>) => {
    if (setValue) setValue(e.target.value.trim());
    if (onBlur) onBlur(e);
  };

  const mainChange = (e: FocusEvent<HTMLInputElement>) => {
    if (setValue) setValue(e.target.value);
    if (onChange) onChange(e);
  };

  return (
    <label htmlFor={name} className={`relative flex flex-col ${className}`}>
      <span className={classLabel}>{label}</span>
      <input
        type={type}
        name={name}
        className={`px-3 py-2 text-black inter-text ${inputClass} ${
          icon ? 'border rounded-full' : ''
        }`}
        placeholder={placeholder}
        onChange={mainChange}
        onBlur={mainBlur}
        value={value}
      />
      {icon &&
        (iconAction || submit ? (
          <Button className={`${iconClass} absolute top-1`} submit={submit}>
            <img className="w-8" src={icon} alt={alt} />
          </Button>
        ) : (
          <img
            className={`${iconClass} absolute w-8 top-1.5`}
            src={icon}
            alt={alt}
          />
        ))}

      <div className="text-red-500 text-left">{error?.message}</div>
    </label>
  );
};

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  label: 'default',
  classLabel: 'text-transparent absolute -top-5',
  inputClass: '',
  error: {},
  onChange: () => {},
  onBlur: () => {},
  icon: null,
  iconAction: null,
  alt: 'default',
  iconClass: '',
  setValue: () => {},
  value: '',
  className: '',
  submit: false,
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  classLabel: PropTypes.string,
  inputClass: PropTypes.string,
  error: PropTypes.instanceOf(Object),
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  icon: PropTypes.string,
  iconAction: PropTypes.func,
  alt: PropTypes.string,
  iconClass: PropTypes.string,
  setValue: PropTypes.func,
  value: PropTypes.string,
  className: PropTypes.string,
  submit: PropTypes.bool,
};

export default Input;
