import Image from 'next/image';
import PropTypes from 'prop-types';

export interface IBgImage {
  src: string;
  alt: string;
  width: string;
  height: string;
  children?: JSX.Element;
  childrenClass?: string;
  className?: string;
  priority?: boolean;
  gradient?: boolean;
}

const BgImage: React.FC<IBgImage> = ({
  src,
  alt,
  width,
  height,
  children,
  childrenClass,
  className,
  priority,
  gradient,
}) => (
  <div style={{ position: 'relative', width, height }} className={className}>
    <Image
      src={src}
      alt={alt}
      layout="fill"
      objectFit="cover"
      quality={75}
      priority={priority}
    />
    {gradient && (
      <div className="absolute w-full h-full bg-gradient-to-r rounded-xl from-black to-transparent" />
    )}
    <div className={`absolute z-10 ${childrenClass}`}>{children}</div>
  </div>
);

BgImage.defaultProps = {
  children: <div></div>,
  childrenClass: '',
  className: '',
  priority: false,
  gradient: false,
};

BgImage.propTypes = {
  children: PropTypes.element,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  childrenClass: PropTypes.string,
  className: PropTypes.string,
  priority: PropTypes.bool,
  gradient: PropTypes.bool,
};

export default BgImage;
