import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import BgImage from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const button = shallow(
      <BgImage
        src="/assets/images/host.jpeg"
        alt="testing"
        width="100px"
        height="100px"
      />
    );
    expect(shallowToJson(button)).toMatchSnapshot();
  });
});
