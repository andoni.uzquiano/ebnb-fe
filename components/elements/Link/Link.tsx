import PropTypes from 'prop-types';

interface ILink {
  children: string;
  to: string;
  className?: string;
}

const Link: React.FC<ILink> = ({ children, to, className }) => (
  <a
    className={`ml-1 border-b border-transparent ${className}`}
    target="_new"
    rel="noopener noreferrer"
    href={to}
  >
    {children}
  </a>
);

Link.defaultProps = {
  className: '',
};

Link.propTypes = {
  children: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Link;
