import Calendar from 'react-calendar';
import PropTypes from 'prop-types';
import { Dispatch, SetStateAction } from 'react';
interface ICustomCalendar {
  setDate: Dispatch<SetStateAction<Date | Date[]>>;
  date: Date | Date[];
  className?: string;
  showDoubleView?: boolean;
}

const CustomCalendar: React.FC<ICustomCalendar> = ({
  setDate,
  date,
  className,
  showDoubleView,
}) => (
  <Calendar
    className={`${className} text-xs text-e-gray-400`}
    onChange={setDate}
    value={date}
    minDate={new Date()}
    tileClassName="text-sm text-black"
    locale="es"
    showDoubleView={showDoubleView}
    showNeighboringMonth={false}
    selectRange={true}
  />
);

CustomCalendar.defaultProps = {
  className: '',
  showDoubleView: false,
};

CustomCalendar.propTypes = {
  setDate: PropTypes.func.isRequired,
  date: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.instanceOf(Date).isRequired).isRequired,
    PropTypes.instanceOf(Date).isRequired,
  ]).isRequired,
  className: PropTypes.string,
  showDoubleView: PropTypes.bool,
};

export default CustomCalendar;
