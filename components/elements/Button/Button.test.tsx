import { shallow } from 'enzyme';
import React from 'react';
import { configure } from 'enzyme';
import ReactAdapter from '@wojtekmaj/enzyme-adapter-react-17';
import { shallowToJson } from 'enzyme-to-json';

import Button from './index';

configure({ adapter: new ReactAdapter() });
describe('With Enzyme', () => {
  it('should render correctly', () => {
    const button = shallow(<Button>Testing</Button>);
    expect(shallowToJson(button)).toMatchSnapshot();
  });
});
