import PropTypes from 'prop-types';
import Spinner from '../Spinner';

interface IButton {
  children: string | JSX.Element;
  submit: boolean;
  loading: boolean;
  action: () => void;
  className?: string;
  secondary?: boolean;
  hover?: string;
}

const Button = ({
  children,
  submit,
  loading,
  action,
  className,
  secondary,
  hover,
}: IButton) => (
  <button
    type={submit ? 'submit' : 'button'}
    disabled={loading}
    className={`${
      className ||
      `flex items-center transition ${
        secondary
          ? 'bg-white text-black border-black border'
          : 'bg-e-pink text-white'
      } px-6 py-3 rounded-xl inter-semibold hover:bg-opacity-80 duration-300 ${hover}`
    }`}
    onClick={action}
  >
    {loading && <Spinner className="mr-2" />}
    {children}
  </button>
);

Button.defaultProps = {
  submit: false,
  loading: false,
  action: () => {},
  className: null,
  secondary: false,
  hover: '',
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  submit: PropTypes.bool,
  loading: PropTypes.bool,
  action: PropTypes.func,
  className: PropTypes.string,
  secondary: PropTypes.bool,
  hover: PropTypes.string,
};

export default Button;
