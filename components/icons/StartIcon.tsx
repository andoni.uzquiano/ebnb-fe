import React from 'react';

function StartIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="18"
      fill="none"
      viewBox="0 0 20 18"
    >
      <path
        fill="#ED198E"
        d="M9.561.805a.5.5 0 01.878 0l2.563 4.699a.5.5 0 00.347.252l5.26.985a.5.5 0 01.272.835l-3.677 3.89a.5.5 0 00-.133.407l.689 5.308a.5.5 0 01-.71.516l-4.836-2.295a.5.5 0 00-.428 0L4.95 17.697a.5.5 0 01-.71-.516l.689-5.308a.5.5 0 00-.133-.408L1.12 7.576a.5.5 0 01.272-.835l5.26-.985a.5.5 0 00.347-.252l2.563-4.7z"
      ></path>
    </svg>
  );
}

export default StartIcon;
