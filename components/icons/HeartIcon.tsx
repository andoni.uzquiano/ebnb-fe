interface IHeartIcon {
  className?: string;
  color: string;
}

const HeartIcon: React.FC<IHeartIcon> = ({ className, color }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="28"
    height="24"
    fill="none"
    viewBox="0 0 28 24"
    className={className}
  >
    <path
      fill={color}
      d="M24.88 2.667A8.387 8.387 0 0014 1.813a8.36 8.36 0 00-10.88 12.64l8.28 8.294a3.707 3.707 0 005.2 0l8.28-8.294a8.36 8.36 0 000-11.786zM23 12.613l-8.28 8.28a1.014 1.014 0 01-1.44 0L5 12.573a5.72 5.72 0 010-8 5.693 5.693 0 018 0 1.334 1.334 0 001.893 0 5.693 5.693 0 018 0 5.72 5.72 0 01.107 8v.04z"
    ></path>
  </svg>
);

export default HeartIcon;
