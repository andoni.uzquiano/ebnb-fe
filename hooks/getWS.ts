import { useEffect, useState } from 'react';
const getWS = () => {
  const [width, setWidth] = useState(0);
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    setWidth(window.innerWidth);
    setScroll(window.scrollY);
    const onResize = () => setWidth(window.innerWidth);
    const onScroll = () => setScroll(window.scrollY);

    window.addEventListener('resize', onResize);
    window.addEventListener('scroll', onScroll);

    return () => {
      window.removeEventListener('resize', onResize);
      window.removeEventListener('scroll', onScroll);
    };
  }, []);

  return { width, scroll };
};

export default getWS;
