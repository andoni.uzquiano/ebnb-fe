import { SIGN_IN, SIGN_OUT, HANDLE_MODAL } from './types';

export const signIn = (data: { employee_code: string; token: string }) => ({
  type: SIGN_IN,
  payload: data,
});

export const signOut = () => ({
  type: SIGN_OUT,
  payload: {},
});

export const handleModal = (data: boolean) => ({
  type: HANDLE_MODAL,
  payload: data,
});
