import { createStore, Reducer, applyMiddleware, Middleware } from 'redux';
import { MakeStore, createWrapper } from 'next-redux-wrapper';
import { IState } from '@typescript/interfaces';
import reducers from './reducers';
import thunkMiddleware from 'redux-thunk';
const { composeWithDevTools } = require('redux-devtools-extension');

const bindMiddleware = (middleware: Middleware[]) => {
  if (process.env.NODE_ENV !== 'production') {
    return composeWithDevTools(applyMiddleware(...middleware));
  }

  return applyMiddleware(...middleware);
};

const makeStore: MakeStore<IState> = () =>
  createStore(
    reducers as Reducer,
    undefined,
    bindMiddleware([thunkMiddleware])
  );

export const wrapper = createWrapper<IState>(makeStore, { debug: true });
