import { AnyAction } from 'redux';
import { HYDRATE } from 'next-redux-wrapper';
import { SIGN_OUT, SIGN_IN } from '../actions/types';

const INITIAL_STATE = { isAuthenticated: false };

const userReducer = (state = INITIAL_STATE, { type, payload }: AnyAction) => {
  switch (type) {
    case HYDRATE:
      return { ...state, payload };
    case SIGN_OUT:
      return { isAuthenticated: false, user: payload };
    case SIGN_IN:
      return { isAuthenticated: true, user: payload };
    default:
      return state;
  }
};

export default userReducer;
