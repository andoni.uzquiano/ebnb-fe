import { combineReducers, AnyAction, CombinedState } from 'redux';
import user from './userReducer';
import modal from './modalReducer';

const initialState = {
  user: { isAuthenticated: false },
};

const appReducer = combineReducers({
  user,
  modal,
});

export default appReducer;
