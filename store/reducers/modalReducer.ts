import { AnyAction } from 'redux';
import { HYDRATE } from 'next-redux-wrapper';
import { HANDLE_MODAL } from '../actions/types';

const INITIAL_STATE = { open: false };

const userReducer = (state = INITIAL_STATE, { type, payload }: AnyAction) => {
  switch (type) {
    case HYDRATE:
      return { ...state, payload };
    case HANDLE_MODAL:
      return { open: payload };
    default:
      return state;
  }
};

export default userReducer;
